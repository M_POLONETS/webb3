<?php
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
  if (!empty($_GET['save'])) {
    // Если есть параметр save, то выводим сообщение пользователю.
  // Включаем содержимое файла form.php.
  print('Вы были услышаны. Форма сохранена');
}
  // Завершаем работу скрипта.
  exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Проверяем ошибки.

$errors = FALSE;
if (empty($_POST['imya'])) {
  print('Введите имя.<br/>');
  $errors = TRUE;
}

if (empty($_POST['email'])) {
  print('Введите email.<br/>');
  $errors = TRUE;
}

if (empty($_POST['gender'])) {
  print('Выберете пол.<br/>');
  $errors = TRUE;
}

if (empty($_POST['god'])) {
  print('Выберете год рождения.<br/>');
  $errors = TRUE;
}

if (empty($_POST['konechnosti'])) {
  print('Выберете кол-во конечностей.<br/>');
  $errors = TRUE;
}

if (empty($_POST['sily'])) {
  print('Выберете суперспособность.<br/>');
  $errors = TRUE;
}

if ($errors) {
  exit();
}

// Сохранение в базу данных.

$user = 'u20984';
$pass = '7942068';
$db = new PDO('mysql:host=localhost;dbname=u20984', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
$imya = $_POST['imya'];
$email = $_POST['email'];
$god = $_POST['god'];
$gender = $_POST['gender'];
$konechnosti = $_POST['konechnosti'];
$sily = $_POST['sily'];
$biography = $_POST['biography'];


// Подготовленный запрос. Не именованные метки.
try {
  $stmt = $db->prepare("INSERT INTO `user` (`imya`, `email`, `god`, `gender`, `konechnosti`, `biography`) VALUES (:imya, :email, :year_of_birth, :gender, :num_of_limbs, :biography)");
  $stmt -> execute(array(':imya' => $imya, ':email' => $email, ':god' => $god, ':gender' => $gender, ':konechnosti' => $konechnosti, ':biography' => $biography));
  $stmt = $db->prepare("SELECT `id` FROM `user` WHERE imya = :imya AND email = :email");
  $stmt->execute(array(':imya' => $imya, ':email' => $email));
  $result_user = $stmt->fetchAll();
  $sp_id = array();
  $result_sp_map;
  for($i=0; $i<count($sily); $i++)
  {
    $stmt = $db->prepare("SELECT `sp_id` FROM `super_power_map` WHERE sp_name = :sily");
    $stmt ->execute(array(':sily' => $sily[$i]));
    $sp_id[$i] = $stmt->fetchColumn();
  }
  
  for($i=0; $i<count($sily); $i++)
  {
    $stmt = $db->prepare("INSERT INTO `sily` VALUES (:result_user, :sp_id)");
    $stmt ->execute(array(':result_user' => $result_user[0]["id"], ':sp_id' => $sp_id[$i]));
  }
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}


header('Location: ?save=1');
?>
